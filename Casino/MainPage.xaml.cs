﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;



/// <summary>
/// Was developed by Igor Grishchenko
/// </summary>
namespace Casino
{
    public sealed partial class MainPage : Page
    {
        private DispatcherTimer _timer;
        private Gambler _player;
        private Random _randommizer;

        public MainPage()
        {
            this.InitializeComponent();
            _timer = new DispatcherTimer();
            _timer.Tick += dispatcherTimerTick;
            _timer.Interval = TimeSpan.FromMilliseconds(50);
            _randommizer = new Random();

            //Starts the timer(never stops it)
            _timer.Start();

            //Check if PlayerList.txt exists
            CheckFile();
        }

        /// <summary>
        /// Go to the lobby if user enter correct info
        /// </summary>
        private async void OnLogin(object sender, RoutedEventArgs e)
        {
            double cash = 0;
            bool exist = true;
            bool isnumber = double.TryParse(_cashBox.Text, out cash);

            //Check if the amount is valid
            if (isnumber == true && double.Parse(_cashBox.Text) != 0 && double.Parse(_cashBox.Text) > 0)
            {
                StorageFolder storageFile = ApplicationData.Current.LocalFolder;
                StorageFile sampleFile = await storageFile.GetFileAsync("PlayerList.txt");
                IList<string> playerList = await FileIO.ReadLinesAsync(sampleFile);
                if (playerList.Any() == true)
                {
                    for (int iPlayer = 0; iPlayer < playerList.Count; iPlayer++)
                    {
                        if (playerList[iPlayer] == _nameBox.Text)
                        {
                            exist = false;
                        }
                    }
                }
                if (exist == true)
                {
                    _player = new Gambler(_nameBox.Text, cash);
                    this.Frame.Navigate(typeof(Lobby), _player);
                    _player.SaveUser();
                    SavePlayerNames();
                }
                else
                {
                    MessageDialog msg = new MessageDialog($"Player with name: {_nameBox.Text} is already exist.");
                    await msg.ShowAsync();
                }
            }
            else
            {
                MessageDialog dialog = new MessageDialog("Please enter a valid amount", "Invalid Value");
                await dialog.ShowAsync();
            }


        }

        /// <summary>
        /// Changes the "Enter" button's color
        /// </summary>
        public void dispatcherTimerTick(object sender, object e)
        {
             int possibility = _randommizer.Next(0, 5);
             switch (possibility)
             {
                  case 0:
                    _btnLogin.Foreground = new SolidColorBrush(Windows.UI.Colors.Orange);
                    break;
                  case 1:
                    _btnLogin.Foreground = new SolidColorBrush(Windows.UI.Colors.DarkGoldenrod);
                    break;
                  case 2:
                    _btnLogin.Foreground = new SolidColorBrush(Windows.UI.Colors.Red);
                    break;
                case 3:
                    _btnLogin.Foreground = new SolidColorBrush(Windows.UI.Colors.Gold);
                    break;
                case 4:
                    _btnLogin.Foreground = new SolidColorBrush(Windows.UI.Colors.Yellow);
                    break;
            }
        }


        /// <summary>
        /// Saves player names by adding them into the Txt file
        /// </summary>
        private async void SavePlayerNames()
        {
            StorageFolder storageFile = ApplicationData.Current.LocalFolder;

            StorageFile sampleFile = await storageFile.GetFileAsync("PlayerList.txt");

            await FileIO.AppendTextAsync(sampleFile, $"{_player.Name}\n");
        }

        /// <summary>
        /// Check if file exist 
        /// If not, program creates it 
        /// </summary>
        private async void CheckFile()
        {
            StorageFolder storageFile = ApplicationData.Current.LocalFolder;
            try
            {
                var file = await storageFile.GetFileAsync("PlayerList.txt");
            }
            catch
            {
                StorageFile SampleFile = await storageFile.CreateFileAsync("PlayerList.txt");
            }
            
        }

    }
}
