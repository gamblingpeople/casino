﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


/// <summary>
/// Was developed by Juraj Proner
/// </summary>
namespace Casino
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Roulette : Page
    {
        private Random _randomizer;
        private List<bool> _betList;
        private List<double> _amountList;
        private Gambler _player;
        List<double> _spins;
        public Roulette()
        {
            _randomizer = new Random();
            this.InitializeComponent();
            _betList = new List<bool>();
            _amountList = new List<double>();
            _player = null;
            _spins = new List<double>();
            for(int i = 0; i<=22; i++)
            {
                _betList.Add(false);
                _amountList.Add(0);
            }
            
        }
        
        /// <summary>
        /// Creates the BackButton and allows user go back to the lobby
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _player = e.Parameter as Gambler;

            //setup the navigate BACK
            SystemNavigationManager navBtn = SystemNavigationManager.GetForCurrentView();
            _txtCash.Text = $"$ {_player.Cash.ToString()}";
            //Display the back button
            navBtn.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;

            //setup the back event handler called when the user clicks the back button
            navBtn.BackRequested += OnNavigateBack;
        }

        private void OnNavigateBack(object sender, BackRequestedEventArgs e)
        {
            //ask the frame if back navigaton is possible and go back 
            if (this.Frame.CanGoBack)
            {
                _player.CheckSave("Roulette", _spins);

                //go exactly to the lobby
                this.Frame.Navigate(typeof(Lobby), _player);
                e.Handled = true;
            }
        }

        private void On0(object sender, RoutedEventArgs e)
        {
            PlaceBet("0", 0);
        }

        private async void PlaceBet(string bet, int index)
        {
            int amount = 0;
            bool result = int.TryParse(_txtBet.Text, out amount);
            if (result == true && amount <= _player.Cash)
            {
                _player.PlaceBetForRoulette(amount, index);
                _amountList[index] = amount;
                _betList[index] = true;
                _txtBlkBetDesc.Text += $"${amount} on {bet}. ";
            }
            else
            {
                MessageDialog dialog = new MessageDialog("Invalid bet or insuficient funds!");
                await dialog.ShowAsync();
            }
        }

        private void On1(object sender, RoutedEventArgs e)
        {
            PlaceBet("1", 1);
        }

        private void On2(object sender, RoutedEventArgs e)
        {
            PlaceBet("2", 2);
        }

        private void On3(object sender, RoutedEventArgs e)
        {
            PlaceBet("3", 3);
        }

        private void On4(object sender, RoutedEventArgs e)
        {
            PlaceBet("4", 4);
        }

        private void On5(object sender, RoutedEventArgs e)
        {
            PlaceBet("5", 5);
        }

        private void On6(object sender, RoutedEventArgs e)
        {
            PlaceBet("6", 6);
        }

        private void On7(object sender, RoutedEventArgs e)
        {
            PlaceBet("7", 7);
        }

        private void On8(object sender, RoutedEventArgs e)
        {
            PlaceBet("8", 8);
        }

        private void On9(object sender, RoutedEventArgs e)
        {
            PlaceBet("9", 9);
        }

        private void On10(object sender, RoutedEventArgs e)
        {
            PlaceBet("10", 10);
        }

        private void On11(object sender, RoutedEventArgs e)
        {
            PlaceBet("11", 11);
        }

        private void On12(object sender, RoutedEventArgs e)
        {
            PlaceBet("12", 12);
        }

        private void On13(object sender, RoutedEventArgs e)
        {
            PlaceBet("13", 13);
        }

        private void On14(object sender, RoutedEventArgs e)
        {
            PlaceBet("14", 14);
        }

        private void On15(object sender, RoutedEventArgs e)
        {
            PlaceBet("15", 15);
        }
        const int FIRSTROW = 16;
        private void On1stRow(object sender, RoutedEventArgs e)
        {
            PlaceBet("1st Row", FIRSTROW);
        }
        const int SECONDROW = 17;
        private void On2ndRow(object sender, RoutedEventArgs e)
        {
            PlaceBet("2nd Row", SECONDROW);
        }
        const int THIRDROW = 18;
        private void On3rdRow(object sender, RoutedEventArgs e)
        {
            PlaceBet("3rd Row", THIRDROW);
        }
        const int EVEN = 19;
        private void OnEven(object sender, RoutedEventArgs e)
        {
            PlaceBet("Even", EVEN);
        }
        const int RED = 20;
        private void OnRed(object sender, RoutedEventArgs e)
        {
            PlaceBet("Red", RED);
        }
        const int BLACK = 21;
        private void OnBlack(object sender, RoutedEventArgs e)
        {
            PlaceBet("Black", BLACK);
        }
        const int ODD = 22;
        private void OnOdd(object sender, RoutedEventArgs e)
        {
            PlaceBet("Odd", ODD);
        }

        private async void OnSpin(object sender, RoutedEventArgs e)
        {
            _btnSpin.IsEnabled = false;
            int number = 0;
            for (int i = 0; i < 1000; i++)
            {
                 number = _randomizer.Next(0, 15);
                _txtblkWinningNumber.Text = number.ToString();
                await Task.Delay(5);
            }
            DetermineWinner(number);

        }
        private void DetermineWinner(int number)
        {
            if(_betList[number] == true)
            {
                _player.CashOutForRoullette(number, 16);
            }
            if(number%2 == 0 && _betList[EVEN] == true)
            {
                _player.CashOutForRoullette(EVEN, 16/8);
            }
            if (number % 2 == 0 && _betList[BLACK] == true && number != 0)
            {
                _player.CashOutForRoullette(BLACK, 15 / 7);
            }

            if (number%2 == 1 && _betList[ODD] == true)
            {
                _player.CashOutForRoullette(ODD, 15/8);
            }
            if (number % 2 == 1 && _betList[RED] == true)
            {
                _player.CashOutForRoullette(RED, 15 / 8);
            }
            if (number == 1 || number == 4 || number == 7 || number == 10 || number == 13 && _betList[FIRSTROW] == true)
            {
                _player.CashOutForRoullette(FIRSTROW, 16 / 5);
            }
            if (number == 2 || number == 5 || number == 8 || number == 11 || number == 14 && _betList[SECONDROW] == true)
            {
                _player.CashOutForRoullette(SECONDROW, 16 / 5);
            }
            if (number == 3 || number == 6 || number == 9 || number == 12 || number == 15 && _betList[SECONDROW] == true)
            {
                _player.CashOutForRoullette(THIRDROW, 16 / 5);
            }
            _spins.Add(_player.AmountWonInRoulette);
            _txtCash.Text = $"$ {_player.Cash.ToString()}";
            _txtwinnings.Text = $"$ {_player.AmountWonInRoulette.ToString()}";
            for (int i = 0; i <= 22; i++)
            {
                _betList[i] = false;
                _amountList[i] = 0;
            }
            _txtBlkBetDesc.Text = "";
             _btnSpin.IsEnabled = true;

            //Leave if lost all money
            _player.Leave("Slots", _spins);

        }
    }
}
