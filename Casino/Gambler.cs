﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;


/// <summary>
/// Was developed by Juraj Proner and Igor Grishchenko
/// </summary>
namespace Casino
{
    //Delegate
    public delegate void LeaveGame(string gameName, List<double> winAmount);


    public class Gambler
    {
        //Declaration of the filed variables 
        private string _playerName;
        private double _playerCash;
        private int _playerBet;
        List<int> _betListRoulette;
        private double _amountWonInRoulette;
        private LeaveGame _leaving { get; set; }


        /// <summary>
        /// Constructor of the Gambler Class
        /// </summary>
        public Gambler(string name, double cash)
        {
            _amountWonInRoulette = 0;
            _playerName = name;
            _playerCash = cash;
            _playerBet = 0;
            _betListRoulette = new List<int>();
            for (int i = 0; i <= 22; i++)
            {
                _betListRoulette.Add(0);
            }

        }

        /// <summary>
        /// Roulette amount property
        /// </summary>
        public double AmountWonInRoulette
        {
            get { return _amountWonInRoulette; }
            set { _amountWonInRoulette = value; }
        }

        /// <summary>
        /// Property for the leave method
        /// </summary>
        public LeaveGame Leave
        {
            get { return _leaving; }
            set { _leaving = value; }
        }

        /// <summary>
        /// Naame property
        /// </summary>
        public string Name
        {
            get { return _playerName; }
            set { _playerName = value; }
        }

        /// <summary>
        /// Cash property
        /// </summary>
        public double Cash
        {
            get { return _playerCash; }
            set { _playerCash = value; }
        }

        public int Bet
        {
            get { return _playerBet; }
        }

        /// <summary>
        /// Gambler gets money if (s)he has won
        /// The odds is how much they won based on odds
        /// </summary>
        public double GetMoney(bool win, double odds)
        {
            if(win == true)
            {
                _playerCash += _playerBet*odds;
                return _playerBet * odds;
            }
            else
            {
                _playerCash -= _playerBet;
                return -_playerBet;
            }
        }

        /// <summary>
        /// Gambler allows to place a bet 
        /// </summary>
        public void PlaceBet(int amount)
        {
            _playerBet = amount;
        }

        public void PlaceBetForRoulette(int amount, int index)
        {

            _betListRoulette[index] += amount;
            _playerCash -= amount;
        }
        public void CashOutForRoullette(int number, double odds)
        {
            _playerCash += _betListRoulette[number] * odds;
            _amountWonInRoulette += _betListRoulette[number] * odds;
        }

        public void MakeBetszeroforRoulette()
        {
            for (int i = 0; i <= 22; i++)
            {
                _betListRoulette[i] = 0;
            }
        }

        /// <summary>
        /// Save gambler information
        /// </summary>
        public async void SaveUser()
        {
            StorageFolder storageFile = ApplicationData.Current.LocalFolder;

            StorageFile sampleFile = await storageFile.CreateFileAsync($"{_playerName}.txt", CreationCollisionOption.ReplaceExisting);

            await FileIO.WriteTextAsync(sampleFile, $"{_playerName}\n");
            await FileIO.AppendTextAsync(sampleFile, $"{_playerCash}\n");
        }


        /// <summary>
        /// Saves the game score 
        /// </summary>
        public async void SaveGameScore(string gameName, List<double> amount)
        {
            double bestAmount = amount.Max();
            
            if (bestAmount != 0)
            {
                StorageFolder storageFile = ApplicationData.Current.LocalFolder;

                StorageFile sampleFile = await storageFile.GetFileAsync($"{_playerName}.txt");

                await FileIO.AppendTextAsync(sampleFile, $"{gameName} {bestAmount}\n");
            }
        }

        /// <summary>
        /// Check if player played at least one time
        /// </summary>
        public void CheckSave(string gameName, List<double> wonAmount)
        {
            if (wonAmount.Any() == true)
            {
                SaveGameScore(gameName, wonAmount);
            }
        }
    }
}
