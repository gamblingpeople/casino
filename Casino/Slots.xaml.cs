﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;


/// <summary>
/// Was developed by Igor Grishchenko
/// </summary>
namespace Casino
{
    public sealed partial class Slots : Page
    {
        //Declaration of the filed variables 
        private Gambler _player;
        private DispatcherTimer _dispatcherTimer;
        private SlotMachine _slots;
        private List<double> _wonAmount;

        int count, boxA, boxB, boxC;

        public Slots()
        {
            this.InitializeComponent();

            _dispatcherTimer = new DispatcherTimer();
            _dispatcherTimer.Tick += dispatcherTimer_Tick;
            _dispatcherTimer.Interval = TimeSpan.FromMilliseconds(10);
            _player = null;
            _wonAmount = new List<double>();
        }

        /// <summary>
        /// Creates the BackButton and allows user go back to the lobby
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //initialize the aggregation relationship with the Gambler 
            _player = e.Parameter as Gambler;
            _userCash.Text = $"You have ${_player.Cash}";

            //setup the navigate BACK
            SystemNavigationManager navBtn = SystemNavigationManager.GetForCurrentView();

            //Display the back button
            navBtn.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;

            //setup the back event handler called when the user clicks the back button
            navBtn.BackRequested += OnNavigateBack;
        }

        private void OnNavigateBack(object sender, BackRequestedEventArgs e)
        {
            //ask the frame if back navigaton is possible and go back 
            if (this.Frame.CanGoBack)
            {
                _player.CheckSave("SLots",_wonAmount);

                //go exactly to the lobby
                this.Frame.Navigate(typeof(Lobby), _player);
                e.Handled = true;
            }
        }

        /// <summary>
        /// Click and start timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnSpin(object sender, RoutedEventArgs e)
        {
            if (_bet.Text != "")
            {
                try
                {
                    if (int.Parse(_bet.Text) > _player.Cash)
                    {
                        MessageDialog msg = new MessageDialog("You do not have enough money for this bet, try again!");
                        await msg.ShowAsync();
                    }
                    else if (int.Parse(_bet.Text) < 0 || int.Parse(_bet.Text) == 0)
                    {
                        MessageDialog msg = new MessageDialog("Invalid amount, try again!");
                        await msg.ShowAsync();
                    }
                    else
                    {
                        //Create Slot machine object 
                        _slots = new SlotMachine(_player, int.Parse(_bet.Text));
                        _dispatcherTimer.Start();
                    }
                }
                catch (FormatException)
                {
                    MessageDialog msg = new MessageDialog("Enter only numbers, try again!");
                    await msg.ShowAsync();
                }
            }
            else
            {
                MessageDialog msg = new MessageDialog("Enter your bet!");
                await msg.ShowAsync();
            }
        }

        /// <summary>
        /// Timer method
        /// Allows images to be changed
        /// Al functions from SLotMachine class
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dispatcherTimer_Tick(object sender, object e)
        {
            count += 10;

            if (count <= 500)
            {
                boxA = _slots.Spin(image_1);
                boxB = _slots.Spin(image_2);
                boxC = _slots.Spin(image_3);
            }
            else
            {
                //Stop
                _dispatcherTimer.Stop();

                _wonAmount.Add(_slots.GetMoney(_slots.Result(boxA, boxB, boxC)));

                //Leave if lost all money
                _player.Leave("Slots", _wonAmount);

                //Update cash label
                _userCash.Text = $"You have ${_player.Cash}";
                
                count = 0;
            }
        }
    }
}
