﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using Windows.Storage;


/// <summary>
/// Was developed by Igor Grishchenko
/// </summary>
namespace Casino
{
    public sealed partial class Lobby : Page
    {
        private Gambler _player;

        public Lobby()
        {
            this.InitializeComponent();

            _player = null;

        }

        //define OnNavigateTo() and imploment it to hide the back button
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //initialize the aggregation relationship with the Gambler 
            _player = e.Parameter as Gambler;

            //Connect the Leave(Gambler) method with LostMoney method (DELEGATE)
            _player.Leave = LostMoney;

            //Call this metod if gambler's cash = 0
            CloseEnter();

            //hide the back button
            SystemNavigationManager navBtn = SystemNavigationManager.GetForCurrentView();
            navBtn.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
        }

        /// <summary>
        /// Go to Slots page
        /// </summary>
        private void OnSlots(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Slots), _player);
        }

        /// <summary>
        /// Go to Roulette page
        /// </summary>
        private void OnRoulette(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Roulette), _player);
        }

        /// <summary>
        /// Go to BlackJack page
        /// </summary>
        private void OnBlackJack(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(BlackJack), _player);
        }

        /// <summary>
        /// If user run out of money program will collapsed all game buttons 
        /// so it closes access to games
        /// </summary>
        private void CloseEnter()
        {
            if (_player.Cash == 0)
            {
                _btnBlackjack.Visibility = Visibility.Collapsed;
                _btnSlots.Visibility = Visibility.Collapsed;
                _btnRoulette.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// If user lost all his money and he is still in game room
        /// program will move him to the lobby 
        /// </summary>
        public async void LostMoney(string gameName, List<double> winAmount)
        {
            if (_player.Cash == 0)
            {
                if (winAmount.Max() != 0)
                {
                    _player.SaveGameScore(gameName, winAmount);
                }
                this.Frame.Navigate(typeof(Lobby), _player);
                MessageDialog msg = new MessageDialog("You have lost and you are run out of money, you have been moved to the lobby.");
                await msg.ShowAsync();
            }
        }

        /// <summary>
        /// This method loads the all user and the best game they played 
        /// </summary>
        public async void LoadPlayersHistory()
        {
            string str = "";
            StorageFolder storageFile = ApplicationData.Current.LocalFolder;
            StorageFile sampleFile = await storageFile.GetFileAsync("PlayerList.txt");

            IList<string> listOfNames = await FileIO.ReadLinesAsync(sampleFile);

            for (int iGambler = 0; iGambler < listOfNames.Count; iGambler++)
            {
                StorageFolder storageFile_2 = ApplicationData.Current.LocalFolder;

                StorageFile sampleFile_2 = await storageFile.GetFileAsync($"{listOfNames[iGambler]}.txt");

                IList<string> playerInfo = await FileIO.ReadLinesAsync(sampleFile_2);

                List<int> allWin = new List<int>();

                Dictionary<string, int> wins = new Dictionary<string, int>();

                for (int iLine = 2; iLine < playerInfo.Count; iLine++)
                {
                    string[] list = playerInfo[iLine].Split();
                    allWin.Add(int.Parse(list[1]));
                }

                for (int i = 2; i < playerInfo.Count; i++)
                {
                    string[] list = playerInfo[i].Split();
                    wins[$"{list[0]}"] = int.Parse(list[1]);
                }

                foreach (KeyValuePair<string, int> pair in wins)
                {
                    if (allWin.Max() == pair.Value)
                    {
                        str += $"{playerInfo[0]} won {pair.Value} in {pair.Key} \n";
                    }
                }
            }
            MessageDialog msg = new MessageDialog(str, "Players history");
            await msg.ShowAsync();
        }

        /// <summary>
        /// Call LoadBestPlayerData() when user click on
        /// </summary>
        private void OnShowBestPlayer(object sender, RoutedEventArgs e)
        {
            LoadPlayersHistory();
        }

        /// <summary>
        /// Close the application
        /// </summary>
        private void OnExit(object sender, RoutedEventArgs e)
        {
            Application.Current.Exit();
        }
    }
}