﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Popups;


/// <summary>
/// Was developed by Igor Grishchenko
/// </summary>
namespace Casino
{

    class SlotMachine
    {
        //Declaration of the filed variables 
        private Gambler _player;
        private Random _randomizer;
        private int _slotBet;

        private const int TRIPLE = 1, DOUBLE = 2, JACKPOT = 3;

        //Constructor
        public SlotMachine(Gambler player, int slotBet)
        {
            _player = player;
            _randomizer = new Random();
            _slotBet = slotBet;
        }

        /// <summary>
        /// Spin the images randomly
        /// Takes image object as a parameter 
        /// </summary>
        public int Spin(Image img)
        {
            int possibility = _randomizer.Next(1, 6);
            switch (possibility)
            {
                case 1:
                    img.Source = new BitmapImage(new Uri("ms-appx:///Assets/bar_1.png"));
                    return 1;
                case 2:
                    img.Source = new BitmapImage(new Uri("ms-appx:///Assets/bar_2.png"));
                    return 2;
                case 3:
                    img.Source = new BitmapImage(new Uri("ms-appx:///Assets/bar_3.png"));
                    return 3;
                case 4:
                    img.Source = new BitmapImage(new Uri("ms-appx:///Assets/7.png"));
                    return 4;
                case 5:
                    img.Source = new BitmapImage(new Uri("ms-appx:///Assets/diamond.jpg"));
                    return 5;
            }
            return 0;
        }

        /// <summary>
        /// Check the result of the game 
        /// </summary>
        /// <returns></returns>
        public int Result(int boxA, int boxB, int boxC)
        {
            if (boxA == boxB && boxB == boxC && boxA == boxC)
            {
                return TRIPLE;
            }
            else if (boxA == boxB || boxB == boxC)
            {
                return DOUBLE;
            }
            else if (boxA == 1 && boxB == 2 && boxC == 3)
            {
                return JACKPOT;
            }
            return 0;
        }

        /// <summary>
        /// Check if player can get or lose money
        /// </summary>
        /// <param name="win"></param>
        public int GetMoney(int win)
        {
            if (win == TRIPLE)
            {
               _player.Cash += _slotBet * 3;
                PrintString($" Triple! You won ${_slotBet * 3} \n Your cash is ${_player.Cash}");
                return _slotBet * 3;
            }
            else if (win == DOUBLE)
            {
                _player.Cash += _slotBet * 2;
                PrintString($" Double! You won ${_slotBet * 2} \n Your cash is ${_player.Cash}");
                return _slotBet * 2;
            }
            else if (win == JACKPOT)
            {
                _player.Cash += _slotBet * 10;
                PrintString($" JackPot!!! You won ${_slotBet * 10} \n Your cash is {_player.Cash}");
                return _slotBet * 10;
            }
            else
            {
                _player.Cash -= _slotBet;

                if (_player.Cash > 0)
                {
                    PrintString($" You Lost, try again! \n Your cash is {_player.Cash}");
                }

                return 0;
            }
        }

        /// <summary>
        /// Takes string as a parameter and prints it
        /// </summary>
        /// <param name="str"></param>
        public async void PrintString(string str)
        {
            MessageDialog msg = new MessageDialog(str);
            await msg.ShowAsync();
        }
    }
}
