﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;


/// <summary>
/// Was developed by Juraj Proner
/// </summary>
namespace Casino
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BlackJack : Page
    {
        private static readonly BitmapImage s_cardBackImage;
        private CardDeck _cardDeck;
        private Card _firstCard;
        private Card _secondCard;
        private List<Card> _listofCardsDealt;
        int _total;
        int _houseTotal;
        List<Image> _imgList;
        private Gambler _player;
        private List<double> _plays;

        static BlackJack()
        {
            s_cardBackImage = new BitmapImage(new Uri("ms-appx:///Assets/playing-card-back.jpg"));
        }

        public BlackJack()
        {
            _player = null;
            this.InitializeComponent();
            _btnHit.IsEnabled = false;
            _btnStay.IsEnabled = false;
            _cardDeck = null;
            _firstCard = null;
            _secondCard = null;
            _plays = new List<double>();
            _listofCardsDealt = new List<Card>();
            _total = 0;
            _imgList = new List<Image>();
            CreateImageList();

        }

        private void CreateImageList()
        {
            _imgList.Add(_imgCard1);
            _imgList.Add(_imgCard2);
            _imgList.Add(_imgCard3);
            _imgList.Add(_imgCard4);
            _imgList.Add(_imgCard5);
            _imgList.Add(_imgCard6);
            _imgList.Add(_imgCard7);
            _imgList.Add(_imgCard8);
            _imgList.Add(_imgCard9);
            _imgList.Add(_imgCard10);
            _imgList.Add(_imgCard11);
            _imgList.Add(_imgCard12);
        }

        /// <summary>
        /// Creates the BackButton and allows user go back to the lobby
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _player = e.Parameter as Gambler;
            _txtCash.Text = $"$ {_player.Cash.ToString()}";
            //setup the navigate BACK
            SystemNavigationManager navBtn = SystemNavigationManager.GetForCurrentView();

            //Display the back button
            navBtn.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;

            //setup the back event handler called when the user clicks the back button
            navBtn.BackRequested += OnNavigateBack;
        }

        private void OnNavigateBack(object sender, BackRequestedEventArgs e)
        {
            //ask the frame if back navigaton is possible and go back 
            if (this.Frame.CanGoBack)
            {
                _player.CheckSave("BlackJack", _plays);

                //go exactly to the lobby
                this.Frame.Navigate(typeof(Lobby), _player);
                e.Handled = true;
            }
        }

        private async void OnDealCards(object sender, RoutedEventArgs e)
        {

            int bet = 0;
            if(int.TryParse(_txtAmountBet.Text, out bet) && bet <= _player.Cash)
            {
                _btnDeal.IsEnabled = false;
                ResetGame();
                _txtCash.Text = $"$ {_player.Cash.ToString()}";
                _cardDeck = new CardDeck();
                _player.PlaceBet(bet);
                _cardDeck.GetPairOfCards(out _firstCard, out _secondCard);
                await Task.Delay(1000);
                ShowCard(_firstCard, _imgCard1);
                await Task.Delay(1000);
                ShowCard(_secondCard, _imgCard2);
                _listofCardsDealt.Add(_firstCard);
                _listofCardsDealt.Add(_secondCard);
                CheckScore();
                _txtPlayerScore.Text = _total.ToString();
                FlipButtons();
                if (_total == 21)
                {
                    MessageDialog message = new MessageDialog("BLACKJACK!");
                    await message.ShowAsync();
                    _player.GetMoney(true, 1.5);
                    ResetGame();
                }
            }
            else
            {
                MessageDialog message = new MessageDialog("Invalid Value bet!");
                await message.ShowAsync();
            }
        }

        private void OnStay(object sender, RoutedEventArgs e)
        {
            _btnStay.IsEnabled = false;
            _btnHit.IsEnabled = false;
            BlackJackAI();

            
        }

        private async void OnHit(object sender, RoutedEventArgs e)
        {
            Card card = null;
            _cardDeck.GetCard(out card);
            _listofCardsDealt.Add(card);
            foreach(Image image in _imgList)
            {
                if (image.Source == null)
                {
                    ShowCard(card, image);
                    break;
                }
            }
            
            CheckScore();
            _txtPlayerScore.Text = _total.ToString();
            if (_total > 21)
            {
                MessageDialog message = new MessageDialog("BUST!");
                _player.GetMoney(false, 1);
                await message.ShowAsync();
                _txtCash.Text = $"$ {_player.Cash.ToString()}";
                _plays.Add(0);
                _player.Leave("BlackJack",_plays);
                ResetGame();
            }
        }

        private void ResetGame()
        {

            _total = 0;
            _txtPlayerScore.Text = _total.ToString();
            _houseTotal = 0;
            _txtHouseScore.Text = _houseTotal.ToString();
            foreach (Card card in _listofCardsDealt)
            {
                card.Value = 0;
            }
            foreach (Image image in _imgList)
            {
                image.Source = null;
            }

            FlipButtons();
        }

        private void FlipButtons()
        {
            if(_btnDeal.IsEnabled == true)
            {
                _btnDeal.IsEnabled = false;
                _btnHit.IsEnabled = true;
                _btnStay.IsEnabled = true;
            }
            else
            {
                _btnDeal.IsEnabled = true;
                _btnHit.IsEnabled = false;
                _btnStay.IsEnabled = false;
            }
            
        }

        private void ShowCard(Card card, Image imageCtrl)
        {
            char suitId = card.Suit.ToString()[0];
            string cardValueId =  card.Name.ToString("00");

            string cardImageFileName = $"{suitId}{cardValueId}.png";

            //display the image with the given file name
            string cardImgPath = $"ms-appx:///Assets/{cardImageFileName}";
            imageCtrl.Source = new BitmapImage(new Uri(cardImgPath));
        }
        private void CheckScore()
        {
            _total = 0;
            foreach(Card card in _listofCardsDealt)
            {

                _total += card.Value;
            }
        }
        private async void BlackJackAI()
        {
            _houseTotal = 0;
            Card housecard1 = null;
            Card housecard2 = null;
            _cardDeck.GetPairOfCards(out housecard1, out housecard2);
            ShowCard(housecard1, _imgCard7);
            _houseTotal += housecard1.Value;
            UpdateLabel();
            await Task.Delay(1000);
            ShowCard(housecard2, _imgCard8);
            _houseTotal += housecard2.Value;
            UpdateLabel();
            await Task.Delay(1000);
            while (_houseTotal < 17)
            {
                Card housecard3 = null;
                _cardDeck.GetCard(out housecard3);
                UpdateLabel();
                for (int i = 6; i < 12; i++)
                {
                    if (_imgList[i].Source == null)
                    {
                        ShowCard(housecard3, _imgList[i]);
                        _houseTotal += housecard3.Value;
                        break;
                    }
                }
                await Task.Delay(1000);


            }
            UpdateLabel();

            await DetermineWinner();
            _btnDeal.IsEnabled = true;
        }

        private async Task DetermineWinner()
        {
            if (_total > 21 || _total < _houseTotal && _houseTotal <= 21)
            {
                MessageDialog message = new MessageDialog("HOUSE WON!");
                await message.ShowAsync();
                OnVictory(false, 1);

            }
            else if (_total > _houseTotal && _houseTotal <= 21 || _houseTotal > 21)
            {
                MessageDialog message = new MessageDialog("PLAYER WON!");
                await message.ShowAsync();
                OnVictory(true, 1);
            }
            else if (_total == _houseTotal)
            {
                MessageDialog message = new MessageDialog("TIE!");
                await message.ShowAsync();
                OnVictory(true, 0);
            }
        }

        private void OnVictory(bool win, int multi)
        {
            double amountWon = 0;
            if (win == true)
            {
                 amountWon = _player.GetMoney(win, multi);
                _plays.Add(amountWon);
            }
            else
            {
                _plays.Add(amountWon);
                _player.Cash -= double.Parse(_txtAmountBet.Text);
                _player.Leave("BlackJack", _plays);
            }
            _txtAmountWon.Text = amountWon.ToString();
            _btnDeal.IsEnabled = true;
            _btnHit.IsEnabled = false;
            _btnStay.IsEnabled = false;
            _txtCash.Text = $"$ {_player.Cash.ToString()}";
        }

        private void UpdateLabel()
        {
            _txtHouseScore.Text = _houseTotal.ToString();
        }
    }
}
