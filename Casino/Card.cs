﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Was developed by Juraj Proner
/// </summary>
namespace Casino
{
    enum CardSuit
    {
        diamond,
        club,
        heart,
        spade
    }

    class Card
    {
        int _name;
        #region Constructors
        /// <summary>
        /// Constructor that requires the a value and suit to create a card
        /// </summary>
        /// <param name="value">The number value for the card</param>
        /// <param name="suit">The suit of the card</param>
        public Card(byte value, CardSuit suit)
        {
            _name = value;
            if (value <= 10 && value > 1)
            {
                _value = value;
            }
            else if(value > 10)
            {
                _value = 10;
            }
            else if(value == 1)
            {
                _value = 11;
            }
            _suit = suit;
        }
        #endregion
        #region Field Variables

        /// <summary>
        /// HAS-A relatioship shown in the UML diagram as an attribute of Card
        /// </summary>
        private byte _value;

        /// <summary>
        /// HAS-A relationship shown in the UML diagram as an association relationship
        /// </summary>
        private CardSuit _suit;

        /// <summary>
        /// Constant field variable, by definition it is static
        /// </summary>
        public const int MAX_CARD_VALUE = 13;

        /// <summary>
        /// Constant field variable, by definition it is static
        /// </summary>
        public const int MAX_SUIT_COUNT = 4;

        #endregion

        

        #region Properties

        public byte Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public CardSuit Suit
        {
            get { return _suit; }
            set { _suit = value; }
        }

        /// <summary>
        /// The name of the card returns a string based on the _value field variable:
        ///    - Ace for 1
        ///    - King for 13
        ///    - Queen for 12
        ///    - Jack for 11
        ///    - the card value as a string for the rest of the cards
        /// </summary>
        public int Name
        {
            get
            {
                return _name;
            }
        }

        #endregion

    }
}
