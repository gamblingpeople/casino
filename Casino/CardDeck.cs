﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Was developed by Juraj Proner
/// </summary>
namespace Casino
{
    class CardDeck
    {
        //The deck as a list of cards
        private List<Card> _cardList;

        /// <summary>
        /// Randomizer used to extract random cards
        /// </summary>
        private static Random s_randomizer;

        /// <summary>
        /// Static constructor to initialize static field variables. Called only once,
        /// the very first time the card game class is used
        /// </summary>
        static CardDeck()
        {
            //initialize the static field variable
            s_randomizer = new Random();
        }


        //Define a constructor the Game class to create the deck
        public CardDeck()
        {
            //create the card deck object
            _cardList = new List<Card>();

            //add the cards to the deck
            CreateCards();
        }

        /// <summary>
        /// Static read-only property
        /// </summary>
        public static Random Randomizer
        {
            get { return s_randomizer; }
        }


        public int CardCount
        {
            get { return _cardList.Count; }
        }

        /// <summary>
        /// add the cards to the deck using a counted loop (for)
        /// </summary>
        private void CreateCards()
        {
            //repeat for every suit value (4 suits)
            for (int iSuit = 0; iSuit < Card.MAX_SUIT_COUNT; iSuit++)
            {
                //get the suit for iSuit
                CardSuit suit = (CardSuit)iSuit;

                //repeat for every card value (13 values)
                for (byte value = 1; value <= Card.MAX_CARD_VALUE; value++)
                {
                    //create the card object
                    
                    Card card = new Card(value, suit);

                    //add the card object to the list
                    _cardList.Add(card);
                }
            }
        }

        public void ShuffleDeck()
        {
            //re-arrange the card list to randomize the deck (i.e.  for the amount
            //of cards in the deck, move the positions of the card randomly). How? 
            //go through each position in the deck (card index) and
            //choose a random card from the deck to place there
            for (byte iDeckPos = 0; iDeckPos < _cardList.Count; iDeckPos++)
            {
                //pick a random card from a position higher than the current position
                int randIndex = Randomizer.Next(iDeckPos, _cardList.Count);

                //swap the randomly chosen card (the card at the random index) with
                //the card in the current position
                Card crtCard = _cardList[iDeckPos];
                _cardList[iDeckPos] = _cardList[randIndex];
                _cardList[randIndex] = crtCard;
            }
        }

        /// <summary>
        /// Extracts two random cards out of the deck
        /// </summary>
        /// <param name="cardOne">output parameter representing the first card extracted</param>
        /// <param name="cardTwo">output parameter representing the second card extracted</param>
        /// <returns>
        ///     - true: if two cards were extracted from the deck
        ///     - false: if the deck does not have two cards
        /// </returns>
        public bool GetPairOfCards(out Card cardOne, out Card cardTwo)
        {
            if (_cardList.Count >= 2)
            {
                //extract the first card
                int randIndex = Randomizer.Next(_cardList.Count);
                cardOne = _cardList[randIndex];
                _cardList.RemoveAt(randIndex);

                //extract the second card
                randIndex = Randomizer.Next(_cardList.Count);
                cardTwo = _cardList[randIndex];
                _cardList.RemoveAt(randIndex);

                //extraction was successful
                return true;
            }
            else
            {
                //extraction was not successful
                cardOne = null;
                cardTwo = null;
                return false;
            }
        }

        public bool GetCard(out Card card)
        {
            if (_cardList.Count >= 1)
            {
                //extract the first card
                int randIndex = Randomizer.Next(_cardList.Count);
                card = _cardList[randIndex];
                _cardList.RemoveAt(randIndex);

                //extraction was successful
                return true;
            }
            else
            {
                //extraction was not successful
                card = null;
                return false;
            }

        }

    }
}
